FROM  node:10.16.3-alpine
WORKDIR /app
COPY . .
RUN npm i
CMD  [  "npm", "run", "start" ]
