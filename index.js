import express from 'express';
require('dotenv').config();

const app = express();
const router = express.Router();

router.get('/', (req, res) => { console.log("AC 05 DevOps"); return res.send("Salve"); });

app.use(router);

app.listen(process.env.PORT, () => console.log(`App listening on port ${process.env.PORT}`));